﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace DataLayer
{
   public interface  IGameDAL
    {

        List<GameEntities> List();
        List<GameEntities> List(int page, int pageSize, string seachValue);
        int Count(string seachValue);
        GameEntities Get(int GamId);
        int Add(GameEntities data);
        bool Update(GameEntities data);
        bool Delete(int GameId);
        List<GameEntities> ListGame();


        GameEx GetEx(int gameId);
        List<Description> ListDescription(int gameId);
        Description GetDescription(long desId);
        long AddDes(Description data);
        bool UpdateDes(Description data);
        bool DeleteDesCription(long attributeID);
   
        List<Image> ListImage(int gameId);
        Image GetImage(long imageId);
        long AddImage(Image data);
        bool UpdateImage(Image data);
        bool DeleteImage(long galleryID);
    }
}
