﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace DataLayer
{
    public interface IGameCategoryDAL
    {
        List<GameCategory> ListCategories();

        List<GameCategory> ListGame();

        List<GameCategory> List(int page, int pageSize, int categoryID, int GameId, string seachValue);

        int Count(int categoryID, int gameId, string seachValue);

        GameCategory ListCategories(int gameCategoryId);

        int Add(GameCategory data);



        bool Update(GameCategory data);


        bool Delete(int gameCategoryId);
    }
}
