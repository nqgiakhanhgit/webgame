﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data;
using System.Data.SqlClient;

namespace DataLayer.SQLServer
{

    public class CategoryDAL : _BaseDAL, ICategory
    {
        public CategoryDAL(string connectionString) : base(connectionString)
        {

        }
        public int Add(Categories data)
        {
            int CategoryId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO dbo.Category
        ( CategoryName 
        )

VALUES  (
		@CategoryName
        )
select @@IDENTITY"
;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);

                CategoryId = Convert.ToInt32(cmd.ExecuteScalar());// 1 dong 1 cot 1 gia tri
                connection.Close();
            }
            return CategoryId;
        }

        public int Count(string seachValue)
        {
            if (seachValue != "")
            {
                seachValue = "%" + seachValue + "%";
            }
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select count (*)  from Category where @seachValue like N'' or CategoryName like @seachValue";
                cmd.Parameters.AddWithValue("@seachValue", seachValue);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                count = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return count;
        }

        public bool Delete(int categoryID)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE  FROM dbo.Category where categoryId = @categoryId
                    AND NOT EXISTS(SELECT * FROM GameCategory
				WHERE CategoryID = Category.CategoryID)";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@categoryId", categoryID);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }

        public Categories Get(int categoryId)
        {
            Categories data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select * from Category Where CategoryID = @categoryId";
                cmd.Parameters.AddWithValue("@categoryId", categoryId);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Categories()
                        {
                            CategoryId = Convert.ToInt32(dbReader["CategoryID"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),
                        };
                    }

                    connection.Close();
                }

            }
            ///reader
            return data;
        }

        public List<Categories> List(int page, int pageSize, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<Categories> data = new List<Categories>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from
                                    (
                                        select *, ROW_NUMBER() OVER(ORDER by CategoryName) as RowNumber

                                        from Category

                                        where (@searchValue = '')

                                            OR(
                                                CategoryName LIKE @searchValue

                                        
                                                
                                            )
                                    ) AS s
                                    where s.RowNumber between(@page -1)*@pageSize + 1 and @page*@pageSize
                                    Order by s.RowNumber";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);

                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Categories()
                        {
                            CategoryId = Convert.ToInt32(dbReader["CategoryId"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),

                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public bool Update(Categories data)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE dbo.Category 
SET CategoryName =@CategoryName

	
	WHERE CategoryID = @CategoryID";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryId);
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }
    }
}
