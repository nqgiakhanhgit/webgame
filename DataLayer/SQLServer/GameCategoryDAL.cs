﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace DataLayer.SQLServer
{
    public class GameCategoryDAL : _BaseDAL, IGameCategoryDAL
    {
        public GameCategoryDAL(string connectionString) : base(connectionString)
        {

        }
        public int Add(GameCategory data)
        {
            int gamecategoryId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO GameCategory
                                    (
	                                   GameId,
                                       CategoryId
                                    )
                                    VALUES
                                    (
	                                 
	                                    @gameId,
                                        @categoryId,
                                   
                                    )
                                    SELECT @@IDENTITY";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;


                cmd.Parameters.AddWithValue("@gameId", data.GameId);
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryId);



                gamecategoryId = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return gamecategoryId;
        }

        public int Count(int categoryID, int gameId, string searchValue)
        {
            int count = 0;
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT  COUNT(*)

                                        FROM    GameCategory 
                                        WHERE   (@categoryId = 0 OR CategoryId = @categoryId)
                                        AND  (@gameid = 0 OR GameId = @gameid)
                                        AND (@searchValue = '' )";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@categoryId", categoryID);
                cmd.Parameters.AddWithValue("@gameid", gameId);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }

        public bool Delete(int gameCategoryId)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM GameCategory
                                            WHERE(GameCategoryId = @gamecategoryId)
                                             ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@gamecategoryId", gameCategoryId);

                int rowsAffected = cmd.ExecuteNonQuery();
                result = rowsAffected > 0;
                connection.Close();
            }
            return result;
        }


        public GameCategory ListCategories(int gameCategoryId)
        {
            GameCategory data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM GameCategory WHERE GameCategoryId = @gameCategoryId";
                cmd.Parameters.AddWithValue("@gameCategoryId", gameCategoryId);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new GameCategory()
                        {
                            GameCategoryId = Convert.ToInt32(dbReader["GameCategoryId"]),
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            CategoryId = Convert.ToInt32(dbReader["CategoryId"]),
     
                        };
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<GameCategory> List(int page, int pageSize, int categoryID, int gameId, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<GameCategory> data = new List<GameCategory>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT  *
                                    FROM
                                    (
                                        SELECT  GameCategory.*, ROW_NUMBER() OVER(ORDER BY GameCategoryId) AS RowNumber,CategoryName,GameName
                                        FROM    GameCategory inner join Category on GameCategory.CategoryId = Category.CategoryId  inner join Game on GameCategory.GameId = Game.GameId
                                        WHERE   (@categoryId = 0 OR GameCategory.CategoryId = @categoryId)
                                        AND  (@gameId = 0 OR GameCategory.GameId = @gameId)
                                        AND (@searchValue = '' or Game.GameName like @searchValue)
                                    ) AS s
                                    WHERE s.RowNumber BETWEEN (@page - 1)*@pageSize + 1 AND @page*@pageSize";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@categoryId", categoryID);
                cmd.Parameters.AddWithValue("@gameId", gameId);
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new GameCategory()
                        {
                            GameCategoryId = Convert.ToInt32(dbReader["GameCategoryId"]),
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            CategoryId = Convert.ToInt32(dbReader["CategoryId"]),
                            GameName = Convert.ToString(dbReader["GameName"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<GameCategory> ListCategories()
        {
            List<GameCategory> data = new List<GameCategory>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM category ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new GameCategory()
                        {
                            CategoryId = Convert.ToInt32(dbReader["CategoryId"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),

                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<GameCategory> ListGame()
        {
            List<GameCategory> data = new List<GameCategory>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Game";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new GameCategory()
                        {
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            GameName = Convert.ToString(dbReader["GameName"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public bool Update(GameCategory data)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {

                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE GameCategory
                                            SET GameId = @GameId,
                                                CategoryId = @CategoryId
                                    WHERE GameCategoryId = @gameCategoryId";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                cmd.Parameters.AddWithValue("@gameCategoryId", data.GameCategoryId);
                cmd.Parameters.AddWithValue("@GameId", data.GameId);
                cmd.Parameters.AddWithValue("@CategoryId", data.CategoryId);


                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }
            return result;
        }
    }
}
