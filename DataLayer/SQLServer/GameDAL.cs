﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using System.Data;
using System.Data.SqlClient;
using Entities;

namespace DataLayer.SQLServer
{
    public class GameDAL : _BaseDAL, IGameDAL
    {
        public GameDAL(string connectionString) : base(connectionString)
        {

        }

        public int Add(GameEntities data)
        {
            int GameId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT dbo.Game
                                            (
                                                GameName,
                                                Capacity,
                                                Price,
                                                ImageThumb,
                                                detal,
                                                LinkgameAdroid,
                                                LinkgamePC,
                                                FileParth
                                        
    
                                            )
                                            VALUES
                                            (   @GameName,
                                                @Capacity, 
                                                @Price  ,
                                                @detal,
                                                @linkgameAdroid,
                                                @linkgamePc,
                                                @imageThumb,
                                                @fileParth
                                                ); SELECT @@IDENTITY;";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@GameName", data.GameName);
                cmd.Parameters.AddWithValue("@Capacity", data.Capacity);
                cmd.Parameters.AddWithValue("@Price", data.Price);
                cmd.Parameters.AddWithValue("@detal", data.detail);
                cmd.Parameters.AddWithValue("@linkgameAdroid", data.linkAdroid);
                cmd.Parameters.AddWithValue("@linkgamePc", data.linkPc);
                cmd.Parameters.AddWithValue("@imageThumb", data.Imagethumb);
                cmd.Parameters.AddWithValue("@fileParth", data.fileParth);
                GameId = Convert.ToInt32(cmd.ExecuteScalar());// 1 dong 1 cot 1 gia tri
                connection.Close();
            }
            return GameId;
        }

        public int Count(string seachValue)
        {
            if (seachValue != "")
            {
                seachValue = "%" + seachValue + "%";
            }
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select count (*)  from Game where @gamename like N'' or GameName like @gamename ";
                cmd.Parameters.AddWithValue("@gamename", seachValue);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                count = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }
            return count;
        }

        public bool Delete(int GameId)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE  FROM dbo.Game where GameId = @GameId
                    AND (GameId NOT IN(SELECT GameId FROM DescriptionGame)) 
                    AND(GameId NOT IN(SELECT GameId FROM DescriptionGame))";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@GameId", GameId);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }

        public GameEntities Get(int GameID)
        {
            GameEntities data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "Select * from Game Where GameID = @GameID";
                cmd.Parameters.AddWithValue("@GameID", GameID);
                cmd.CommandType = System.Data.CommandType.Text;
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new GameEntities()
                        {
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            GameName = Convert.ToString(dbReader["GameName"]),
                            Capacity = Convert.ToString(dbReader["Capacity"]),
                            Price = Convert.ToDouble(dbReader["Price"]),
                            Imagethumb = Convert.ToString(dbReader["Imagethumb"]),
                            linkPc = Convert.ToString(dbReader["LinkgamePC"]),
                            linkAdroid = Convert.ToString(dbReader["LinkgameAdroid"]),
                            detail = Convert.ToString (dbReader["detal"]),
                       
                            fileParth = Convert.ToString(dbReader["FileParth"])
                        };
                    }

                    connection.Close();
                }

            }
            ///reader
            return data;
        }

        public List<GameEntities> List()
        {
            List<GameEntities> data = new List<GameEntities>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select * from Game";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new GameEntities()
                        {
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            GameName = Convert.ToString(dbReader["GameName"]),
                            Capacity = Convert.ToString(dbReader["Capacity"]),
                            Price = Convert.ToDouble(dbReader["Price"]),
                            Imagethumb = Convert.ToString(dbReader["Imagethumb"]),
                            linkPc = Convert.ToString(dbReader["LinkgamePC"]),
                            linkAdroid = Convert.ToString(dbReader["LinkgameAdroid"]),
                            detail = Convert.ToString(dbReader["detal"]),
                            
                            fileParth = Convert.ToString(dbReader["FileParth"])

                        });
                    }
                }
                connection.Close();
            }
            return data;
        }
        public List<GameEntities> ListGame()
        {
            List<GameEntities> data = new List<GameEntities>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Game";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new GameEntities()
                        {
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            GameName = Convert.ToString(dbReader["GameName"]),
                            Capacity = Convert.ToString(dbReader["Capacity"]),
                            Price = Convert.ToDouble(dbReader["price"]),
                            Imagethumb = Convert.ToString(dbReader["ImageThumb"]),
                            linkAdroid = Convert.ToString(dbReader["LinkgameAdroid"]),
                            linkPc = Convert.ToString(dbReader["LinkgamePc"]),
                            detail  = Convert.ToString(dbReader["detal"]),
                        
                            fileParth = Convert.ToString(dbReader["FileParth"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }
        public List<GameEntities> List(int page, int pageSize, string searchValue)
        {
            if (searchValue != "")
                searchValue = "%" + searchValue + "%";

            List<GameEntities> data = new List<GameEntities>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from
                                    (
                                        select *, ROW_NUMBER() OVER(ORDER by GameName) as RowNumber

                                        from Game

                                        where (@searchValue = '')

                                            OR(
                                                GameName LIKE @searchValue

                                         
                                                
                                            )
                                    ) AS s
                                    where s.RowNumber between(@page -1)*@pageSize + 1 and @page*@pageSize
                                    Order by s.RowNumber";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new GameEntities()
                        {
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            GameName = Convert.ToString(dbReader["GameName"]),
                            Capacity = Convert.ToString(dbReader["Capacity"]),
                            Price = Convert.ToDouble(dbReader["Price"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public bool Update(GameEntities data)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE dbo.Game 
    SET         GameName = @GameName,
                Capacity = @Capacity,
                Price = @Price,
                detal = @delal,
                LinkgameAdroid = @linkgameAdroid,
                LinkgamePC = @linkgamePc,
                ImageThumb = @imagethumb,
FileParth =@fileparth
                Where GameId = @GameId ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@GameId", data.GameId);
                cmd.Parameters.AddWithValue("@GameName", data.GameName);
                cmd.Parameters.AddWithValue("@Capacity", data.Capacity);
                cmd.Parameters.AddWithValue("@Price", data.Price);
                cmd.Parameters.AddWithValue("@delal", data.detail);
                cmd.Parameters.AddWithValue("@linkgameAdroid", data.linkAdroid);
                cmd.Parameters.AddWithValue("@linkgamePc", data.linkPc);
                cmd.Parameters.AddWithValue("@imagethumb", data.Imagethumb);
                cmd.Parameters.AddWithValue("@fileparth", data.fileParth);

                int rowAffected = cmd.ExecuteNonQuery();
                result = rowAffected > 0;
                connection.Close();
            }
            return result;
        }


       public List<GameCategory> ListCategoryOfGame(int gameId)
        {

            List<GameCategory> data = new List<GameCategory>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"                                                SELECT  GameCategory.*,CategoryName,GameName,Price,Capacity
                                        FROM    GameCategory inner join Category on GameCategory.CategoryId = Category.CategoryId  inner join Game on GameCategory.GameId = Game.GameId
                                        WHERE  
                                         ( GameCategory.GameId = @gameId)
                                 ";
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@gameId", gameId);

                cmd.Connection = connection;

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new GameCategory()
                        {
                         
                            GameId = Convert.ToInt32(dbReader["GameId"]),
                            CategoryId = Convert.ToInt32(dbReader["CategoryId"]),
                            GameName = Convert.ToString(dbReader["GameName"]),
                            CategoryName = Convert.ToString(dbReader["CategoryName"]),
                            Price = Convert.ToDouble(dbReader["Price"]),
                            Capacity = Convert.ToString(dbReader["Capcity"])

                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public GameEx GetEx(int gameId)
        {
            GameEntities game = Get(gameId);
            if (game == null)
            {
                return null;
            }


            List<Image> listimage = this.ListImage(gameId);
            List<Description> listDescription = this.ListDescription(gameId);
            GameEx data = new GameEx()
            {
                GameId = game.GameId,
                GameName = game.GameName,
                Price = game.Price,
                Capacity = game.Capacity,
                Imagethumb = game.Imagethumb,
                detail = game.detail,
                linkAdroid = game.linkAdroid,
                linkPc = game.linkPc,
                fileParth = game.fileParth,
                listDescriptions = listDescription,
                listImage = listimage
            };

            return data;
        }

        public List<Description> ListDescription(int gameId)
        {
            List<Description> data = new List<Description>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM DescriptionGame 
                                   WHERE GameId = @gameId 
                                   ORDER BY DesciptionId asc";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@gameid", gameId);
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Description()
                        {
                            DesciptionId = Convert.ToInt32(dbReader["DesciptionId"]),
                            DescriptionContent = Convert.ToString(dbReader["DescriptionContent"]),
                            GameId = Convert.ToInt32(dbReader["GameId"])
                        });
                    }
                }

                connection.Close();
            }

            return data;
        }

        public Description GetDescription(long desId)
        {
            Description data = null;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT * FROM DescriptionGame WHERE DescriptionId = @desId";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@desId", desId);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Description()
                        {
                            DesciptionId = Convert.ToInt32(dbReader["DescriptionId"]),
                            DescriptionContent = Convert.ToString(dbReader["DescriptionContent"]),
                            GameId = Convert.ToInt32(dbReader["GameId"])
                        };
                    }
                }

                connection.Close();
            }

            return data;
        }

        public long AddDes(Description data)
        {
            int DescriptionId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO DescriptionGame (GameId, DescriptionContent)
                                            VALUES (@gameId, @DescriptionContent)
                                            SELECT @@IDENTiTY";

                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@gameId", data.GameId);
                cmd.Parameters.AddWithValue("@DescriptionContent", data.DescriptionContent);


                DescriptionId = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }

            return DescriptionId;
        }

        public bool UpdateDes(Description data)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE dbo.DescriptionGame
                                    SET DescriptionContent = @DescriptionContent 
                                           WHERE DesciptionId = @DesciptionId";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@gameId", data.GameId);
                cmd.Parameters.AddWithValue("@DesciptionId", data.DesciptionId);
                cmd.Parameters.AddWithValue("@DescriptionContent", data.DescriptionContent);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        }

        public bool DeleteDesCription(long desCriptionId)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM DescriptionGame WHERE DesciptionId = @desCriptionId";

                cmd.Parameters.AddWithValue("@desCriptionId", desCriptionId);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        
    }

        public List<Image> ListImage(int gameId)
        {
            List<Image> data = new List<Image>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM Image 
                                   WHERE GameId = @gameId 
                                   ORDER BY ImageId asc";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@gameId", gameId);
                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Image()
                        {
                           ImageId = Convert.ToInt32(dbReader["ImageId"]),
                     
                           ImagePath = Convert.ToString(dbReader["ImagePath"]),
                           Gameid = Convert.ToInt32(dbReader["GameId"])
                        });
                    }
                }

                connection.Close();
            }

            return data;
        }

        public Image GetImage(long imageId)
        {
            Image data = null;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT * FROM Image WHERE ImageId = @imageId";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@imageId", imageId);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Image()
                        {
                            ImageId = Convert.ToInt32(dbReader["ImageId"]),
                            ImagePath = Convert.ToString(dbReader["ImageParth"]),
                            Gameid = Convert.ToInt32(dbReader["GameId"])
                        };
                    }
                }

                connection.Close();
            }

            return data;
        }

        public long AddImage(Image data)
        {
            int DescriptionId = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"INSERT INTO Image (ImagePath,GameId)
                                            VALUES ( @imagePath,@gameId)
                                            SELECT @@IDENTiTY";

                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@gameId", data.Gameid);

                cmd.Parameters.AddWithValue("@imagePath", data.ImagePath);


                DescriptionId = Convert.ToInt32(cmd.ExecuteScalar());

                connection.Close();
            }

            return DescriptionId;
        }

        public bool UpdateImage(Image data)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"UPDATE Image 
                                        SET 
                                        GameId = @gameId,
                                 
                                        ImagePath = @imageParth

                                        where ImageId = @imageId";

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@gameId", data.Gameid);
                cmd.Parameters.AddWithValue("@imageId", data.ImageId);
            
                cmd.Parameters.AddWithValue("@imageParth", data.ImagePath);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        }

        public bool DeleteImage(long imageIsd)
        {
            bool result = false;

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM dbo.Image WHERE ImageId = @imageIsd";

                cmd.Parameters.AddWithValue("@imageIsd", imageIsd);

                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }

            return result;
        }
    }
}
