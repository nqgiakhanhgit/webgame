﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.SQLServer
{
   public class _BaseDAL
    {
        /// <summary>
        /// Chuỗi tham số kết nối
        /// </summary>
        protected string _connectionString;

        public _BaseDAL(string connectingString)
        {
            this._connectionString = connectingString;
        }

        /// <summary>
        /// Mở kết nối đến CSDL
        /// </summary>
        /// <returns></returns>
        protected SqlConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = this._connectionString;
            connection.Open();
            return connection;
        }
    }

}
