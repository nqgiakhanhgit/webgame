﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace DataLayer
{
    public interface ICategory
    {



        List<Categories> List(int page, int pageSize, string seachValue);

        int Count(string seachValue);



        Categories Get(int categoryId);



        int Add(Categories data);


        bool Update(Categories data);
        bool Delete(int categoryID);
    }
}
