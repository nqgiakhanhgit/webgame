﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessLayer;
using DataLayer;
using Entities;

namespace WebGameClient.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = DataService.ListGame();
            return View(model);
        }
        [HttpGet]
        public ActionResult Detail(int id)
        {
            ViewBag.Title = "Game Details";
            var model = DataService.GetGameEx(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View("Detail",model);
        }
     
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}