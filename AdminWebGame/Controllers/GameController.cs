﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entities;
using BussinessLayer;
using System.Diagnostics;
using System.IO;

namespace AdminWebGame.Controllers
{
    public class GameController : Controller
    {
        // GET: Game
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(int page = 1, string searchValue = "")
        {
            int pageSize = 25;
            int rowCount = 0;
            var model = DataService.ListGameEntities(page, pageSize, searchValue, out rowCount);
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.RowCount = rowCount;

            int pageCount = rowCount / pageSize;
            if (rowCount % pageSize > 0)
                pageCount += 1;
            ViewBag.PageCount = pageCount;
            return View(model);
        }
        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.Title = "Add Game";
            GameEntities model = new GameEntities()
            {
                GameId = 0
            };
            return View( model);
        }
        /// <summary>
        /// Chỉnh Sửa Nhà Cung Cấp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Update Game";
            var model = DataService.GetGameEx(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View("Edit",model);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            var model = DataService.GetGame(id);
            if (model == null)
                return RedirectToAction("Index");
            if (Request.HttpMethod == "GET")
            {
                return View(model);
            }
            else
            {
                DataService.DeleteGame(id);
                return RedirectToAction("Index");
            }

        }

        public ActionResult Save(GameEntities data, HttpPostedFileBase uploadFile)
        {
 

            if (string.IsNullOrWhiteSpace(data.GameName))
            {

                ModelState.AddModelError("Game Name", "Name Of The Game Is Not Space Or Null");
            }
            if (string.IsNullOrWhiteSpace(data.Capacity))
            {

                ModelState.AddModelError("Capacity", "Capacity not null Or Not String");
            }
            if (string.IsNullOrWhiteSpace(data.Imagethumb))
            {

                ModelState.AddModelError("Imagethumb", "Image Of The Game Is Not Space Or Null");
            }
            if (string.IsNullOrWhiteSpace(data.linkAdroid))
            {

                ModelState.AddModelError("linkAdroid", "linkAdroid not null Or Not String");
            }
            if (string.IsNullOrWhiteSpace(data.linkPc))
            {

                ModelState.AddModelError("linkPc", "linkPc Of The Game Is Not Space Or Null");
            }
            if (string.IsNullOrWhiteSpace(data.detail))
            {

                ModelState.AddModelError("detail", "detail not null Or Not String");
            }
            
            if (!ModelState.IsValid)
            {
                if (data.GameId == 0)
                    return View("Add", data);
                else
                {
                    var editdata = DataService.GetGameEx(data.GameId);
                    return View("Edit", editdata);
                }
            }
            //: new update
            if (uploadFile != null)
            {

                //Tạo folder để upload ảnh lên Server
                string folder = Server.MapPath("~/Upload");
                string fileName = $"{DateTime.Now.Ticks}{Path.GetExtension(uploadFile.FileName)}";
                //cach 2
                // string fileName1 = string.Format("{0}{1}" ,DateTime.Now.Ticks,Path.GetExtension(uploadFile.FileName));

                string filePath = Path.Combine(folder, fileName);
                data.fileParth = fileName;

                uploadFile.SaveAs(filePath);

                if (string.IsNullOrWhiteSpace(data.fileParth))
                {
                    ModelState.AddModelError("fileParth", "File Of The Game Is Not Space Or Null");
                }


            }
            if (data.GameId == 0)
            {

                var maxGameId = DataService.AddGame(data);
                return RedirectToAction("Edit", new { id = maxGameId });
            }
            else
            {
                DataService.UpdateGame(data);
            }

            return RedirectToAction("Edit", new { id = data.GameId });
            //   return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveImage(Image data, HttpPostedFileBase ImageFile)
        {
            if (ImageFile != null)
            {
                //Tạo folder để upload ảnh lên Server
                string folder = Server.MapPath("~/Upload/ListImage");
                string fileName = $"{DateTime.Now.Ticks}{Path.GetExtension(ImageFile.FileName)}";
                //cach 2
                // string fileName1 = string.Format("{0}{1}" ,DateTime.Now.Ticks,Path.GetExtension(uploadFile.FileName));

                string filePath = Path.Combine(folder, fileName);
                data.ImagePath = fileName;

                ImageFile.SaveAs(filePath);

                if (string.IsNullOrWhiteSpace(data.ImagePath))
                {
                    ModelState.AddModelError("Image", "File Image Of The Game Is Not Space Or Null");
                }
            }
        
            if (data.ImageId == 0)
            {
                DataService.AddImageGame(data);
            }
            else
            {
                DataService.UpdateImage(data);
            }
            return RedirectToAction("Edit", new { id = data.ImageId });
        }
        public ActionResult SaveDescription(Description data)
        {
            if (data.DesciptionId == 0)
            {
                DataService.AddDescription(data);
            }
            else
            {

                DataService.UpdateDescription(data);
            }
            return RedirectToAction("Edit", new { id = data.DesciptionId });
        }
    

    public ActionResult DeleteDescription(int id, long[] DescriptionId)
        {
            if (DescriptionId == null || DescriptionId.Length == 0)
            {
                return RedirectToAction("Edit", new { id = id });
            }
            DataService.DeleteDesciption(DescriptionId);
            return RedirectToAction("Edit", new { id = id });
        }
        public ActionResult DeleteImage(int id, long[] imageIds)
        {
            if (imageIds == null || imageIds.Length == 0)
            {
                
                return RedirectToAction("Edit", new { id = id });
            }
            DataService.DeleteImage(imageIds);
            return RedirectToAction("Edit", new { id = id });
        }


    }

}