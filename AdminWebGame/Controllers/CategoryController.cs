﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessLayer;
using Entities;

namespace AdminWebGame.Controllers
{
    public class CategoryController : Controller
    {
        public ActionResult Index()
        {
            return View();

        }
        public ActionResult List(int page = 1, string searchValue = "")
        {
            int pageSize = 25;
            int rowCount = 0;
            List<Categories> categories = DataService.ListCategory(page, pageSize, searchValue, out rowCount);
            Models.CategoryPaginationResult model = new Models.CategoryPaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SeachValue = searchValue,
                Data = categories
            };
            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Thể Loại Game";
            Categories model = new Categories()
            {
                CategoryId = 0
            };
            return View("Edit", model);
        }
        /// <summary>
        /// Chỉnh Sửa Nhà Cung Cấp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Cập Nhật Thông Tin Category";
            Categories model = DataService.GetCategory(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            var model = DataService.GetCategory(id);
            if (model == null)
                return RedirectToAction("Index");
            if (Request.HttpMethod == "GET")
            {
                return View(model);
            }
            else
            {
                DataService.DeleteCategory(id);
                return RedirectToAction("Index");
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Save(Categories data)
        {
            if (string.IsNullOrWhiteSpace(data.CategoryName))
            {

                ModelState.AddModelError("CategoryName", "Tên Không Được để Trống");
            }


            //ModelState Lớp cho phép kiểm soát lỗi
            if (!ModelState.IsValid)
            {
                if (data.CategoryId == 0)
                {
                    ViewBag.Tile = " Bổ Sung Thể Loại";

                }
                else
                    ViewBag.Title = " cập Nhật thông tin Thể loại game";
                return View("Edit", data);
            }
            if (data.CategoryId == 0)
            {

                DataService.AddCategory(data);
            }
            else
            {
                DataService.UpdateCategory(data);
            }
            //return Json(data, JsonRequestBehavior.AllowGet);
            return RedirectToAction("Index", "Category");
        }

    }
}