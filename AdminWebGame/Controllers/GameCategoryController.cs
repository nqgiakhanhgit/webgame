﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using BussinessLayer;
namespace AdminWebGame.Controllers
{
    public class GameCategoryController : Controller
    {
        // GET: GameCategory
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(int page = 1, int category = 0, int gameid = 0, string searchValue = "")
        {
            int rowCount = 0;
            int pageSize = 10;
            var model = GameCategoryService.List(page, pageSize, category, gameid, searchValue, out rowCount);

            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.RowCount = rowCount;

            int pageCount = rowCount / pageSize;
            if (rowCount % pageSize > 0)
                pageCount += 1;
            ViewBag.PageCount = pageCount;

            return View(model);
        }
    }
}