﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminWebGame.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            string connectionString = "server=.\\SQLEXPRESS; user= sa; password = 123123; database=WebGameData";
            var dal = new DataLayer.SQLServer.GameDAL(connectionString);

            return Json(dal.ListCategoryOfGame(1), JsonRequestBehavior.AllowGet);
        }
    }
}