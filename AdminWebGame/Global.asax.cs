﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BussinessLayer;
namespace AdminWebGame
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            string connectionString = ConfigurationManager.ConnectionStrings["WebGameData"].ConnectionString;
            DataService.Init(DatabaseTypes.SQLServer, connectionString);
            GameCategoryService.Init(DatabaseTypes.SQLServer, connectionString);
        }
    }
}
