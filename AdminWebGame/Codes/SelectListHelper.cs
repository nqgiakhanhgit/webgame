﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using BussinessLayer;


namespace AdminWebGame
{
    public static class SelectListHelper
    {
        public static List<SelectListItem> Categories(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "-- All Category Game --",
                });
            }
            foreach (var item in GameCategoryService.ListCategories())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.CategoryId).ToString(),
                    Text = item.CategoryName,
                });
            }

            return list;
        }
            public static List<SelectListItem> Game(int a = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (a != 0)
            {
                list.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = "-- ALL Game --",
                });
            }
            foreach (var item in GameCategoryService.ListGame())
            {
                list.Add(new SelectListItem()
                {
                    Value = (item.GameId).ToString(),
                    Text = item.GameName,
                });
            }

            return list;
        }
    }
}