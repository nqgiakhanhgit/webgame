﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;
namespace AdminWebGame.Models
{
    public class GamePaginationResult: BasePaginationQueryResult
    {
        public List<GameEntities>Data  {get;set;}
    }
}