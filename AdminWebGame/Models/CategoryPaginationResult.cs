﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWebGame.Models
{
    public class CategoryPaginationResult : BasePaginationQueryResult
    {
        public List<Categories> Data
        {
            get; set;
        }
    }
}