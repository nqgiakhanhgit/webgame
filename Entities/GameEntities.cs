﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class GameEntities
    {
        public int GameId { get; set; }
        public string GameName { get; set; }
        public string Capacity { get; set; }
        public double Price
        {
            get; set;
        }
        public string Imagethumb { get; set; }
        public string detail { get; set; }
        public string linkPc { get; set; }
        public string linkAdroid { get; set; }
        public string fileParth
        {
            get; set;
        }
        public string fileParthPC { get; set; }
    } 
    public class GameEx:GameEntities
    {
        public List<Image> listImage { get; set; }
        public List<Description> listDescriptions { get; set; }
    }
}
