﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Description
    {
        public int DesciptionId
        {
            get;
            set;
        }
        public string DescriptionContent
        {
            get;
            set;
        }
        public int GameId
        {
            get;
            set;
        }
    }

}
