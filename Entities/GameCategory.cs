﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public  class GameCategory
    {
        public int GameCategoryId { get; set; }
        public int CategoryId { get; set; }
        public int GameId { get; set; }
        public string GameName { get; set; }
        public string CategoryName { get; set; }
        public double Price { get; set; }
        public string Capacity { get; set; }
    }
    public class GameCategoryEX : GameCategory
    {

        public List<GameCategory> ListGameOfcategory { get; set; }
    }
}
