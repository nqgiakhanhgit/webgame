﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using DataLayer;

namespace BussinessLayer
{
   public static class GameCategoryService
    {
        private static IGameCategoryDAL GameCategoryDB;
        public static void Init(DatabaseTypes dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseTypes.SQLServer:
                    GameCategoryDB = new DataLayer.SQLServer.GameCategoryDAL(connectionString);
                    break;
                default:
                    throw new Exception("Database type is not supported");
            }
        }
        public static List<GameCategory> List(int page, int pageSize,
                               int categoryId, int gameId,
                               string searchValue, out int rowCount)
        {
            rowCount = GameCategoryDB.Count(categoryId, gameId, searchValue);
            return GameCategoryDB.List(page, pageSize, categoryId, gameId, searchValue);
        }
        public static List<GameCategory> ListCategories()
        {
            return GameCategoryDB.ListCategories();
        }

        public static List<GameCategory> ListGame()
        {
            return GameCategoryDB.ListGame();
        }

        public static GameCategory Get(int gamecategoryId)
        {
            return GameCategoryDB.ListCategories(gamecategoryId);
        }
        public static int Add(GameCategory data)
        {
            return GameCategoryDB.Add(data);
        }

        public static bool Update(GameCategory data)
        {
            return GameCategoryDB.Update(data);
        }

        public static bool Delete(int gameCategoryId)
        {
            return GameCategoryDB.Delete(gameCategoryId);
        }
    }
}
