﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using DataLayer;
namespace BussinessLayer
{
   public static class DataService
    {
        private static IGameDAL GameDB;
        private static ICategory CategoryDB;



        public static void Init(DatabaseTypes dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseTypes.SQLServer:
                    GameDB = new DataLayer.SQLServer.GameDAL(connectionString);
                    CategoryDB = new DataLayer.SQLServer.CategoryDAL(connectionString);
                    break;
                case DatabaseTypes.FakeDB:
                    break;
                default:
                    throw new Exception("Database Type is not supported");
            }
        }
        #region Categories
        public static List<Categories> ListCategory(int page, int pageSize, string seachValue, out int rowCount)
            {
                if (page <= 0)
                    page = 1;
                if (pageSize <= 0)
                    pageSize = 25;

                rowCount = CategoryDB.Count(seachValue);

                return CategoryDB.List(page, pageSize, seachValue);
            }
            public static Categories GetCategory(int CategoryId)
            {
                return CategoryDB.Get(CategoryId);
            }

            public static int AddCategory(Categories data)
            {
                return CategoryDB.Add(data);
            }

            public static bool UpdateCategory(Categories data)
            {
                return CategoryDB.Update(data);
            }

            public static bool DeleteCategory(int CategoryId)
            {
                return CategoryDB.Delete(CategoryId);
            }
        #endregion
        #region Game
        public static List<GameEntities> ListGameEntities(int page, int pageSize, string seachValue, out int rowCount)
        {
            if (page <= 0)
                page = 1;
            if (pageSize <= 0)
                pageSize = 25;

            rowCount = GameDB.Count(seachValue);

            return GameDB.List(page, pageSize, seachValue);
        }
        public static GameEntities GetGame(int CategoryId)
        {
            return GameDB.Get(CategoryId);
        }
        public static List<GameEntities> ListGame()
        {
            return GameDB.ListGame();
        }
        public static int AddGame(GameEntities data)
        {
            return GameDB.Add(data);
        }

        public static bool UpdateGame(GameEntities data)
        {
            return GameDB.Update(data);
        }

        public static bool DeleteGame(int CategoryId)
        {
            return GameDB.Delete(CategoryId);
        }

        public static long AddImageGame(Image Data)
        {
            return GameDB.AddImage(Data);
        }
        public static long AddDescription (Description data)
        {
            return GameDB.AddDes(data);
        }
        public static GameEx GetGameEx ( int gameid)
        {
            return GameDB.GetEx(gameid);
        }
        public static List<Image> ListImage (int gameid)
        {
            return GameDB.ListImage(gameid);
        }
        public static List<Description> ListDescription(int gameid)
        {
            return GameDB.ListDescription(gameid);
        }
        public static bool UpdateDescription(Description data)
        {
            return GameDB.UpdateDes(data);
        }
        public static void DeleteDesciption(long []descriptionId)
        {
            foreach(var id in descriptionId)
            {
                GameDB.DeleteDesCription(id);
            }
        }
        public static bool UpdateImage(Image data)
        {
            return GameDB.UpdateImage( data);
        }
        public static void DeleteImage(long [] imageId)
        {
            foreach (var id in imageId)
            {
                GameDB.DeleteImage(id);
            }
        }
        #endregion

    }
}
